package pl.mbapps.githubviewer.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_repos.*
import pl.mbapps.githubviewer.R
import pl.mbapps.githubviewer.data.Commit
import pl.mbapps.githubviewer.data.Repository
import pl.mbapps.githubviewer.vm.ReposViewModel
import pl.mbapps.githubviewer.vm.ReposViewModelFactory
import java.net.UnknownHostException

class ReposActivity : AppCompatActivity() {

    private lateinit var viewModel: ReposViewModel
    private lateinit var adapter: RepositoriesViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repos)

        initReposList()

        viewModel = ViewModelProviders.of(this, ReposViewModelFactory()).get(ReposViewModel::class.java)
        registerDataObservers()

        viewModel.reloadRepositories(getString(R.string.github_user))
    }

    private fun registerDataObservers() {
        viewModel.getRepositoriesData().observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                showRepos(it)
                viewModel.downloadLastCommits(getString(R.string.github_user))
            } else {
                showInfo(R.string.error_empty_repos_list)
            }
        })
        viewModel.getErrorData().observe(this, Observer {
            if (it != null && it is UnknownHostException) {
                showInfo(R.string.error_no_connection)
                return@Observer
            }
            if (it != null) {
                showInfo(R.string.error_with_excpetion, it.toString())
            }
        })
        viewModel.getCommitsData().observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                adapter.repoCommits = it
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun initReposList() {
        rvRepos.layoutManager = LinearLayoutManager(this)
        adapter = RepositoriesViewAdapter()
        rvRepos.adapter = adapter
    }

    private fun showRepos(repositories: List<Repository>) {
        adapter.repos = repositories
        tvMsgText.visibility = View.GONE
        rvRepos.visibility = View.VISIBLE
    }

    private fun showInfo(infoId: Int, argument: String? = null) {
        tvMsgText.text = getString(infoId, argument)
        rvRepos.visibility = View.GONE
        tvMsgText.visibility = View.VISIBLE
    }

}
