package pl.mbapps.githubviewer.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.view_single_repo.view.*
import pl.mbapps.githubviewer.R
import pl.mbapps.githubviewer.data.Commit
import pl.mbapps.githubviewer.data.Repository

class RepositoriesViewAdapter : RecyclerView.Adapter<RepositoryViewHolder>() {

    var repos: List<Repository> = ArrayList()
    var repoCommits: Map<Repository, Commit> = HashMap()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_single_repo, parent, false)
        return RepositoryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return repos.size
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        val repo = repos[position]
        holder.bindRepository(repo)
        holder.bindCommit(repoCommits[repo])
    }

}

class RepositoryViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    fun bindRepository(repo: Repository) {
        itemView.repoName.text = repo.name
        itemView.repoStars.text = itemView.context.getString(R.string.stars_text, repo.stars)
        itemView.repoWatchers.text = itemView.context.getString(R.string.watchers_text, repo.watchers)
        itemView.repoForks.text = itemView.context.getString(R.string.forks_text, repo.forks)
    }

    fun bindCommit(commit: Commit?) {
        if (commit == null) {
            itemView.llCommitDetails.visibility = View.GONE
            return
        }

        itemView.tvCommitAuthor.text = itemView.context.getString(R.string.last_commit_author, commit.author)
        itemView.tvCommitMessage.text = itemView.context.getString(R.string.last_commit_message, commit.message)

        itemView.llCommitDetails.visibility = View.VISIBLE
    }
}