package pl.mbapps.githubviewer

import android.app.Application
import pl.mbapps.githubviewer.di.DependenciesFactory
import pl.mbapps.githubviewer.di.DependenicesFactoryImpl

class GithubViewerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        DependenciesFactory.instance = DependenicesFactoryImpl(this)
    }

}