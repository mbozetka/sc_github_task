package pl.mbapps.githubviewer.di

import android.content.Context
import pl.mbapps.githubviewer.service.GithubService
import pl.mbapps.githubviewer.service.GithubServiceImpl

class DependenicesFactoryImpl(context: Context) : DependenciesFactory {

    private val githubServiceImpl: GithubService

    init {
        githubServiceImpl = GithubServiceImpl(context)
    }

    override fun getGithubService(): GithubService {
        return githubServiceImpl
    }

}