package pl.mbapps.githubviewer.di

import pl.mbapps.githubviewer.service.GithubService

interface DependenciesFactory {

    fun getGithubService(): GithubService

    companion object {
        lateinit var instance: DependenciesFactory
    }

}