package pl.mbapps.githubviewer.service

import android.content.Context
import org.json.JSONArray
import pl.mbapps.githubviewer.R
import pl.mbapps.githubviewer.data.Commit
import pl.mbapps.githubviewer.data.Repository
import java.net.URL

class GithubServiceImpl(val context: Context) : GithubService {

    override fun userRepositories(user: String): List<Repository> {
        val urlStr = context.getString(R.string.github_repos_path, user)
        val reposJSONArray = callServiceForJSONArray(urlStr)
        val repos = ArrayList<Repository>()
        for (i in 0 until reposJSONArray.length()) {
            val repoJSONObject = reposJSONArray.getJSONObject(i)
            val parsedRepo = Repository.repositoryFromJSONObject(repoJSONObject)
            repos.add(parsedRepo)
        }
        return repos
    }

    override fun getRepoLastCommit(user: String, repo: String): Commit? {
        val urlStr = context.getString(R.string.github_repo_commits, user, repo)
        val commitsJSONArray = callServiceForJSONArray(urlStr)
        if (commitsJSONArray.length() < 1) {
            return null
        }
        return Commit.commitFromJSONObject(commitsJSONArray.getJSONObject(0))
    }

    private fun callServiceForJSONArray(urlStr: String): JSONArray {
        val url = URL(urlStr)
        val response = url
                .openStream()
                .bufferedReader()
                .use { it.readText() }
        return JSONArray(response)
    }

}