package pl.mbapps.githubviewer.service

import pl.mbapps.githubviewer.data.Commit
import pl.mbapps.githubviewer.data.Repository

interface GithubService {

    fun userRepositories(user: String): List<Repository>

    fun getRepoLastCommit(user: String, repo: String): Commit?

}