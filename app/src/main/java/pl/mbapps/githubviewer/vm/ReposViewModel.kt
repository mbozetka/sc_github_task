package pl.mbapps.githubviewer.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import pl.mbapps.githubviewer.data.Commit
import pl.mbapps.githubviewer.data.Repository
import pl.mbapps.githubviewer.service.GithubService
import java.io.IOException

class ReposViewModel(private val githubService: GithubService) : ViewModel() {

    private val repositoriesData = MutableLiveData<List<Repository>>()
    private val errorData = MutableLiveData<Exception>()
    private val commitsData = MutableLiveData<MutableMap<Repository, Commit>>()

    fun reloadRepositories(user: String) {
        launch(UI) {
            try {
                val repos = async { githubService.userRepositories(user) }.await()
                repositoriesData.value = repos
                errorData.value = null
            } catch (e: Exception) {
                errorData.value = e
            }
        }
    }

    fun downloadLastCommits(user: String) {
        commitsData.value = HashMap()
        launch(UI) {
            repositoriesData.value?.forEach {
                try {
                    val commit = async { githubService.getRepoLastCommit(user, it.name) }.await()
                    if (commit != null) {
                        val commitsMap = commitsData.value!!
                        commitsMap[it] = commit
                        commitsData.value = commitsMap
                    }
                } catch (e: Exception) {
                    println("Exception: " + e)
                }
            }
        }
    }

    fun getRepositoriesData(): LiveData<List<Repository>> {
        return repositoriesData
    }

    fun getErrorData(): LiveData<Exception> {
        return errorData
    }

    fun getCommitsData(): LiveData<MutableMap<Repository, Commit>> {
        return commitsData
    }

}