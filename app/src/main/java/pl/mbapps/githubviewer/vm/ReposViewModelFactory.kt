package pl.mbapps.githubviewer.vm

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import pl.mbapps.githubviewer.di.DependenciesFactory

class ReposViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val githubService = DependenciesFactory.instance.getGithubService()
        return ReposViewModel(githubService) as T
    }

}