package pl.mbapps.githubviewer.data

import org.json.JSONObject

data class Repository(
        val name: String,
        val stars: Int,
        val forks: Int,
        val watchers: Int
) {

    companion object {

        fun repositoryFromJSONObject(obj: JSONObject): Repository {
            return Repository(
                    obj.getString("name"),
                    obj.getInt("stargazers_count"),
                    obj.getInt("forks_count"),
                    obj.getInt("watchers_count")
            )
        }

    }

}