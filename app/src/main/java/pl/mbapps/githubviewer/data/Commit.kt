package pl.mbapps.githubviewer.data

import org.json.JSONObject

data class Commit(
        val author: String,
        val message: String
) {
    companion object {

        fun commitFromJSONObject(obj: JSONObject): Commit {
            return Commit(
                    obj.getJSONObject("commit").getJSONObject("author").getString("name"),
                    obj.getJSONObject("commit").getString("message")
            )
        }

    }
}